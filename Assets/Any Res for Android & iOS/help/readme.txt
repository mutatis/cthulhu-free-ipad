Thanks for purchasing AnyRes! 

I really appreciate your support and hope you find it to be a valuable tool when using Unity!

Youtube tutorial: http://www.youtube.com/watch?v=8kbmZlMXUmA 
note: I added some things since I uploaded the video so check out the documentation.

contact: skatiskate@hotmail.com

Online documentation[1.1]: https://docs.google.com/file/d/0B0zPVuI5PjflR2F0ZzJEcHF0bDg/edit?usp=sharing 

Changes
[1.0] Initial release
[1.1] Added support for GUIText
